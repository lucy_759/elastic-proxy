package com.epam.atlas.esnewbie.service;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import com.epam.atlas.esnewbie.utills.ObjectMapperSingleton;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ElasticClient {
	private Client client;

	public ElasticClient() {
		super();
		openConnection();
	}

	// Logging

	public Client getClient() {
		return client;
	}

	public void logSearchResult(long timeTook, long totalResult, String index, String type) {
		long millis = new Date().getTime();
		logSearchResults("time_took", timeTook, millis, index, type);
		logSearchResults("total_result", totalResult, millis, index, type);
	}

	private void logSearchResults(String tag, long value, long now, String index, String type) {
		ObjectMapper mapper = ObjectMapperSingleton.getInstance();
		Map<String, Object> map = new HashMap<>();
		map.put("tag", tag);
		map.put("value", value);
		map.put("request_time", now);
		try {
			writeLogIntoIndex(mapper.writeValueAsString(map), index, type);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}

	private void writeLogIntoIndex(String json, String index, String type) {
		client.prepareIndex(index, type).setSource(json).get();
		System.out.println("Request successfully logged");
	}

	// Initialization

	public void initializeSystem(String index, String type) throws JsonProcessingException, IOException {
		ObjectMapper mapper = ObjectMapperSingleton.getInstance();
		BulkRequestBuilder bulkRequest = client.prepareBulk();
		JsonNode mainNode = mapper.readTree(new File("new-accounts.json"));
		for (int i = 0; i < mainNode.size(); i += 2) {
			bulkRequest
					.add(client.prepareIndex("account", "simple", mainNode.get(i).get("index").get("_id").textValue())
							.setSource(mainNode.get(i + 1).toString()));
		}
		BulkResponse bulkResponse = bulkRequest.get();
		if (bulkResponse.hasFailures()) {
			System.out.println("Initializatoin failure.");
		}
	}

	private void openConnection() {
		Settings settings = Settings.builder().put("cluster.name", "elasticsearch").build();
		try {
			client = new PreBuiltTransportClient(settings)
					.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("localhost"), 9300));
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}

	public void closeConnection() {
		client.close();
	}
}
