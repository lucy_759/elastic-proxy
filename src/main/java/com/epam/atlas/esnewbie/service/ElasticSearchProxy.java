package com.epam.atlas.esnewbie.service;

import static org.elasticsearch.index.query.QueryBuilders.boolQuery;

import java.io.IOException;
import java.util.Map;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import com.epam.atlas.esnewbie.utills.ObjectMapperSingleton;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ElasticSearchProxy {
	public static final String INDEX = "account";
	public static final String TYPE = "simple";
	public static final String LOG_INDEX = "logs";
	public static final String LOG_TYPE = "simple";
	private ElasticClient connectionManager;

	public ElasticSearchProxy(ElasticClient connectionManager) {
		super();
		this.connectionManager = connectionManager;
	}

	public byte[] searchWithCommandLine(Map<String, Object> params) throws IOException {
		BoolQueryBuilder qb = boolQuery();
		System.out.println("Lite search: ");
		for (Map.Entry<String, Object> param : params.entrySet()) {
			qb.must(QueryBuilders.matchQuery(param.getKey(), param.getValue()));
		}
		SearchResponse response = connectionManager.getClient().prepareSearch(INDEX).setTypes(TYPE)
				.setSearchType(SearchType.DEFAULT).setQuery(qb).get();
		System.out.println("Query execute duration: " + response.getTookInMillis());
		System.out.println("Found: " + response.getHits().getTotalHits() + " results");
		connectionManager.logSearchResult(response.getTookInMillis(), response.getHits().getTotalHits(), LOG_INDEX,
				LOG_TYPE);
		return prettyOutput(response.toString());
	}

	public byte[] searchWithQueryDSL(byte[] body) throws JsonParseException, JsonMappingException, IOException {
		System.out.println("Query DSL search:");
		ObjectMapper mapper = ObjectMapperSingleton.getInstance();
		JsonNode json = mapper.readTree(body);
		QueryBuilder qb = QueryBuilders.wrapperQuery(json.get("query").toString());
		System.out.println(qb.toString());
		int size = 20;
		if (json.get("size") != null) {
			size = json.get("size").asInt();
		}
		int from = 0;
		if (json.get("from") != null) {
			size = json.get("from").asInt();
		}
		SearchResponse response = connectionManager.getClient().prepareSearch(INDEX).setTypes(TYPE)
				.setSearchType(SearchType.DEFAULT).setQuery(qb).setSize(size).setFrom(from).get();
		System.out.println("Query execute duration: " + response.getTook());
		System.out.println("Found: " + response.getHits().getTotalHits() + " results");
		connectionManager.logSearchResult(response.getTookInMillis(), response.getHits().getTotalHits(), LOG_INDEX,
				LOG_TYPE);
		return prettyOutput(response.toString());
	}

	private byte[] prettyOutput(String str) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = ObjectMapperSingleton.getInstance();
		Object json = mapper.readValue(str.getBytes(), Object.class);
		byte[] prettyJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsBytes(json);
		return prettyJson;
	}
}
