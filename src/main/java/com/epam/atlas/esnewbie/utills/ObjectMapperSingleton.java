package com.epam.atlas.esnewbie.utills;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ObjectMapperSingleton {
	private static final ObjectMapper INSTANCE = new ObjectMapper();

	public static ObjectMapper getInstance() {
		return INSTANCE;
	}
}
