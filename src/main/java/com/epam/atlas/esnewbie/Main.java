package com.epam.atlas.esnewbie;

import java.io.IOException;
import java.util.Map;

import org.rapidoid.http.Req;
import org.rapidoid.http.Resp;
import org.rapidoid.setup.On;

import com.epam.atlas.esnewbie.service.ElasticClient;
import com.epam.atlas.esnewbie.service.ElasticSearchProxy;;

public class Main {
	public static void main(String[] args) {
		try {
			new ElasticClient().initializeSystem(ElasticSearchProxy.INDEX, ElasticSearchProxy.TYPE);
		} catch (IOException e) {

			e.printStackTrace();
		}
		On.get("/search").json((Req req) -> {
			System.out.println("Search path recived with proxy: " + req.uri());
			ElasticSearchProxy esp = new ElasticSearchProxy(new ElasticClient());
			Resp resp = req.response();
			Map<String, Object> params = req.data();
			resp.body(esp.searchWithCommandLine(params));
			return resp;
		});
		On.post("/search").json((Req req) -> {
			System.out.println(new String(req.body()));
			Resp resp = req.response();
			byte[] body = req.body();
			ElasticSearchProxy esp = new ElasticSearchProxy(new ElasticClient());
			resp.body(esp.searchWithQueryDSL(body));
			return resp;

		});
	}
}
